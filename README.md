# How to start coding something?

## Разработка в docker контейнерах.

1. Склонировав из GitLab репозитории проект:

```bash
git clone https://gitlab.com/grandcore/grandcore-front-react.git
```

2. Создайте в корне проекта файл `.env` с переменными. Можно воспользоваться файлом `.env.example` скопировав его содержимое в `.env` файл. Закоменченные переменные в файле `.env.example` не я зляются обязательными и имеют теже значения по умолчанию. Т.е. устанавливаются по умолчанию если не заданы. Если требуется изменить интересующей Вас переменную раскоментируйте её и задайте нужные Вам значения.

3. Произведите сборку и запуск сервера:

```bash
docker-compose up --build
```

или можно в фоновом режиме без "полотна" логов

```bash
docker-compose up --build -d
```

После всего сервер должен быть доступен по адресу http://localhost/ и работать в режиме разработки. Что даёт возможность на лету получать изменения на страницах при внесение изменения в коде.

4. Для того, что бы задать пароль администратора воспользуйтесь командой:

```bash
docker exec -it backend python3 manage.py createsuperuser
```

После чего можно зайти в админку по адресу http://localhost/admin указав логин и пароль только что созданного администратора.

5. Для остановки сервера:

```bash
docker-compose stop
```

6. Для остановки сервера и удаления контейнеров:

```bash
docker-compose down -v
```

## Development outside of Docker containers.

### #1 clone the repo

```bash
git clone https://gitlab.com/grandcore/grandcore-front-react.git
```

### #2 install dependencies

```bash
npm install
```

### #3 create `.env.local` file at project's root level and paste `var` here

```bash
NEXT_PUBLIC_BASE_API_URL=http://demo.grandcore.org/api
```

### #4 start development server

```bash
npm run dev
```

### #5 happy coding!

```bash
open http://localhost:3000 in your browser
```

## How to manage your work with other developers?

### WIP

Please, contact us directly on our [discord server](https://discord.gg/BkDMZ8fd), [telegram channel](https://t.me/grandcore_chat) or something else you know.

## Technologies

[![typescript](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/200px-Typescript_logo_2020.svg.png)](https://www.typescriptlang.org/)

[![nextjs](https://upload.wikimedia.org/wikipedia/commons/8/8e/Nextjs-logo.svg)](https://nextjs.org)

[![react](https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/200px-React.svg.png)](https://reactjs.org/)

[![styled-components](https://styled-components.com/nav-logo.png)](https://styled-components.com/)

[![ant.design](https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg)](https://ant.design/)
