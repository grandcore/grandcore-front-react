type GetUserName = (firstName: string, secondName: string) => string;

type GetUserRang = (rang: number) => string;

type GetUserSkills = (skills: string | Array<string>) => string;

export const getUserName: GetUserName = (firstName, secondName) => {
  if (firstName && secondName) {
    return firstName + ' ' + secondName;
  } else if (firstName) {
    return firstName + 'Стандартный';
  } else if (secondName) {
    return 'Пользователь ' + secondName;
  } else {
    return 'Пользователь Стандартный';
  }
};

export const getUserRang: GetUserRang = (rang) => {
  if (rang) {
    return 'Участник';
  } else {
    return 'undefined';
  }
};

export const getUserSkills: GetUserSkills = (skills) => {
  switch (typeof skills) {
    case 'string':
      return skills;
    case 'object':
      return skills.join(', ');
  }
};
