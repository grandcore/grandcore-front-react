import { IUsersResponse } from './../types/user.types';
import { useState } from 'react';
import { userAPI } from './../services/userApi';

export const useGetUsers = () => {
  const [users, setUsers] = useState<IUsersResponse | undefined>(undefined);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const getUsers = () => {
    setIsLoading(true);

    userAPI.getUsers().then((response) => {
      setIsLoading(false);

      setUsers(response);
    });
  };

  return { users, getUsers, isLoading };
};
