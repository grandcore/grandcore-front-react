import { authAPI } from './../services/authAPI';
import { IAuthBody } from './../types/auth.types';

export const useAuth = (body: IAuthBody) =>
  authAPI.login(body).then((response) => {
    localStorage.setItem('accessToken', JSON.stringify(response));

    return response;
  });
