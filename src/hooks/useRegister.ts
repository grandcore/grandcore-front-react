import { registerAPI } from '../services/registerAPI';
import { IRegisterBody } from '../types/registration.types';

export const useRegister = (body: IRegisterBody) => registerAPI.register(body).then((response) => response);
