import React, { FC } from 'react';
import { StyledForm } from '../common/Form/StyledForm';
import { Button, Form, Input } from 'antd';
import { useAuth } from '../../hooks/useAuth';
import { IAuthBody } from '../../types/auth.types';
import { useRouter } from 'next/router';
import { routes } from '../../routes';

export const AuthForm: FC = () => {
  const router = useRouter();
  const onFinish = (formData: IAuthBody) => useAuth(formData).then(() => router.replace(routes.home));

  return (
    <StyledForm onFinish={onFinish}>
      <Form.Item name="email" rules={[{ required: true, type: 'email' }]}>
        <Input placeholder="Email..." />
      </Form.Item>

      <Form.Item name="password" rules={[{ required: true }]}>
        <Input.Password placeholder="Password..." />
      </Form.Item>

      <Form.Item>
        <Button htmlType="submit" block>
          Авторизироваться
        </Button>
      </Form.Item>
    </StyledForm>
  );
};
