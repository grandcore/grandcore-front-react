import React, { FC } from 'react';
import { StyledForm } from '../common/Form/StyledForm';
import { Button, Form, Input } from 'antd';
import { useRegister } from '../../hooks/useRegister';
import { IRegisterBody } from '../../types/registration.types';
import { useRouter } from 'next/router';
import { routes } from '../../routes';
import { useAuth } from '../../hooks/useAuth';
import { IAuthBody } from '../../types/auth.types';

export const RegisterForm: FC = () => {
  const router = useRouter();
  const onFinish = (formData: IRegisterBody) =>
    useRegister(formData)
      .then(() => {
        delete formData.invite;

        return useAuth(formData as IAuthBody);
      })
      .then(() => router.replace(routes.home));

  return (
    <StyledForm onFinish={onFinish}>
      <Form.Item name="email" rules={[{ required: true, type: 'email' }]}>
        <Input placeholder="Email..." />
      </Form.Item>

      <Form.Item name="password" rules={[{ required: true }]}>
        <Input.Password placeholder="Password..." />
      </Form.Item>

      <Form.Item name="invite" rules={[{ required: true }]}>
        <Input placeholder="Invite..." />
      </Form.Item>

      <Form.Item>
        <Button htmlType="submit" block>
          Зарегистрироваться
        </Button>
      </Form.Item>
    </StyledForm>
  );
};
