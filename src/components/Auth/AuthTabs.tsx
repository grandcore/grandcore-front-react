import React, { FC, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { routes } from '../../routes';
import { StyledTabs } from '../common/Tabs/StyledTabs';
import { Tabs } from 'antd';
import { AuthForm } from './AuthForm';
import { RegisterForm } from './RegisterForm';

export enum EAuthTabs {
  Login = 'login',
  Register = 'registration',
}

interface IComponentProps {}

export const AuthTabs: FC<IComponentProps> = () => {
  const router = useRouter();
  const [activeTabKey, setActiveTab] = useState<string>(EAuthTabs.Login as string);

  useEffect(() => {
    setActiveTab(router.query.activeKey as string);
  }, [router.query.activeKey]);

  const handleOnTabChange = (tabName: string) => {
    router.replace(`${routes.auth}?activeKey=${tabName}`);
  };

  return (
    <StyledTabs onChange={handleOnTabChange} activeKey={activeTabKey}>
      <Tabs.TabPane tab="Вход" key={EAuthTabs.Login}>
        <AuthForm />
      </Tabs.TabPane>

      <Tabs.TabPane tab="Регистрация" key={EAuthTabs.Register}>
        <RegisterForm />
      </Tabs.TabPane>
    </StyledTabs>
  );
};
