import React, { FC, useEffect } from 'react';
import styled from 'styled-components';
import { Spin } from 'antd';
import { useGetUsers } from '../../hooks/useUser';
import { UserList } from '../User/UserList';

const Wrapper = styled.div``;

const Headline = styled.h2`
  margin-bottom: 50px;
  font-weight: 800;
  font-size: 48px;
  color: ${(p) => p.theme.color.gray};
`;

export const Users: FC = () => {
  const { users, getUsers, isLoading } = useGetUsers();

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <>
      {isLoading && <Spin tip="Пользователи загружаются..." />}

      {!isLoading && (
        <Wrapper>
          <Headline children="Участники" />

          <UserList users={users?.results} />
        </Wrapper>
      )}
    </>
  );
};
