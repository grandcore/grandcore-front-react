import React, { FC } from 'react';
import Link from 'next/link';
import { routes } from '../../routes';
import LogoImage from '../../../public/logo.svg';
import styled from 'styled-components';

const Wrapper = styled.div`
  height: 40px;
  width: 120px;
  cursor: pointer;
`;

export const Logo: FC = () => {
  return (
    <Link href={routes.home}>
      <Wrapper>
        <LogoImage />
      </Wrapper>
    </Link>
  );
};
