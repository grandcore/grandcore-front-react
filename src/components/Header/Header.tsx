import React, { FC } from 'react';
import dynamic from 'next/dynamic';
import styled from 'styled-components';
import { Logo } from '../Logo/Logo';

const HeaderWrapper = styled.header`
  padding: 12px ${(p) => p.theme.indent.pdX};
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  background-color: ${(p) => p.theme.color.white};
  border-bottom: 1px solid #d1d1d1;
  position: sticky;
  top: 0;
  z-index: 10;

  > *:first-child {
    margin-right: 10%;
  }
`;

const DynamicHeaderMenu = dynamic(() => import('./HeaderMenu').then((module) => module.HeaderMenu), { ssr: false });

export const Header: FC = () => {
  return (
    <HeaderWrapper>
      <Logo />

      <DynamicHeaderMenu />
    </HeaderWrapper>
  );
};
