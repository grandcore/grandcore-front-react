import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Menu } from 'antd';
import { routes } from '../../routes';
import { EAuthTabs } from '../../components/Auth/AuthTabs';
import { PrimaryButton } from '../common/Button/PrimaryButton';

const MenuWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;

  .ant-menu-horizontal {
    width: 100%;
    display: flex;
    border-bottom: none;

    .ant-menu-item.ant-menu-item-only-child {
      font-size: 18px;
      font-weight: 600;
      color: ${(p) => p.theme.color.gray};
      border-bottom: none;
    }

    .ant-menu-item.ant-menu-item-only-child.ant-menu-item-active {
      color: ${(p) => p.theme.color.accent};

      a:hover {
        color: ${(p) => p.theme.color.accent};
      }
    }

    .ant-menu-item.ant-menu-item-only-child.ant-menu-item-selected {
      color: ${(p) => p.theme.color.accent};

      a {
        color: ${(p) => p.theme.color.accent};
      }
    }
  }
`;

export const HeaderMenu = () => {
  const router = useRouter();
  const [activeMenuItem, setActiveMenuItem] = useState<string>('');

  useEffect(() => setActiveMenuItem(router.pathname.replace(/\//, '')), [router]);

  const handleOnAuthClick = () => {
    router.push(`${routes.auth}?activeKey=${EAuthTabs.Login}`);
  };

  return (
    <MenuWrapper>
      <Menu mode="horizontal" selectedKeys={[activeMenuItem]}>
        <Menu.Item key="soft">
          <Link href={routes.soft}>
            <a>Софт</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="games">
          <Link href={routes.games}>
            <a>Игры</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="online">
          <Link href={routes.online}>
            <a>Онлайн-сервисы</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="products">
          <Link href={routes.products}>
            <a>Изделия</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="texts" style={{ marginRight: 'auto' }}>
          <Link href={routes.texts}>
            <a>Тексты</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="about" style={{ fontWeight: 400 }}>
          <Link href={routes.about}>
            <a>О фонде</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="sponsors" style={{ fontWeight: 400 }}>
          <Link href={routes.sponsors}>
            <a>Спонсоры</a>
          </Link>
        </Menu.Item>

        <Menu.Item key="members" style={{ fontWeight: 400 }}>
          <Link href={routes.members}>
            <a>Участники</a>
          </Link>
        </Menu.Item>

        {/* <Menu.Item key="search">Search</Menu.Item> */}

        <Menu.Item key="auth">
          <PrimaryButton size="large" onClick={handleOnAuthClick}>
            Войти
          </PrimaryButton>
        </Menu.Item>
      </Menu>
    </MenuWrapper>
  );
};
