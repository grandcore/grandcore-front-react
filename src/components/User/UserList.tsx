import React, { FC } from 'react';
import styled from 'styled-components';
import { IUser } from '../../types/user.types';
import { UserItem } from './UserItem';

const List = styled.ul`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: flex-start;
`;

interface IComponentProps {
  users: Array<IUser>;
}

export const UserList: FC<IComponentProps> = ({ users }) => {
  return <List>{Boolean(users?.length) && users?.map((user) => <UserItem user={user} key={user.id} />)}</List>;
};
