import React, { FC } from 'react';
import styled from 'styled-components';
import { getUserSkills } from '../../helpers/user.helpers';

const Wrapper = styled.div``;

interface IComponentProps {
  skills: string | Array<string>;
}

export const UserSkills: FC<IComponentProps> = ({ skills }) => {
  return <Wrapper children={getUserSkills(skills)} />;
};
