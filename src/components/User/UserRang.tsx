import React, { FC } from 'react';
import styled from 'styled-components';
import { getUserRang } from '../../helpers/user.helpers';

const Wrapper = styled.div`
  font-weight: 600;
  font-size: 18px;
  color: ${(p) => p.theme.color.gray3};
`;

interface IComponentProps {
  rang: number;
}

export const UserRang: FC<IComponentProps> = ({ rang }) => {
  return <Wrapper children={getUserRang(rang)} />;
};
