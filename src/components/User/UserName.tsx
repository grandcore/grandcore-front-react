import React, { FC } from 'react';
import styled from 'styled-components';
import { getUserName } from '../../helpers/user.helpers';

const Headline = styled.h3`
  font-weight: 800;
  font-size: 24px;
  text-transform: capitalize;
  color: ${(p) => p.theme.color.gray};
`;

interface IComponentProps {
  firstName: string;
  secondName: string;
}

export const UserName: FC<IComponentProps> = ({ firstName, secondName }) => {
  return <Headline children={getUserName(firstName, secondName)} />;
};
