import React, { FC } from 'react';
import styled from 'styled-components';
import { IUser } from '../../types/user.types';
import { UserAvatar } from './userAvatar';
import { UserName } from './UserName';
import { UserRang } from './UserRang';
import { UserSkills } from './UserSkills';

const ListItem = styled.li`
  margin-bottom: 25px;
  padding: 30px;
  width: calc(50% - 20px);
  background-color: #fafafa;
  border-radius: 8px;

  :nth-child(2n) {
    margin-left: 40px;
  }

  :nth-last-child(-n + 2) {
    margin-bottom: 0;
  }
`;

const Header = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
`;

const HeaderInfo = styled.div`
  margin-left: 30px;
  display: flex;
  flex-flow: column nowrap;
`;

interface IComponentProps {
  user: IUser;
}

export const UserItem: FC<IComponentProps> = ({ user }) => {
  const { first_name: firstName, second_name: secondName, rang, avatar, skills } = user;

  return (
    <ListItem>
      <Header>
        <UserAvatar avatar={avatar} shape="square" size={64} />

        <HeaderInfo>
          <UserName firstName={firstName} secondName={secondName} />

          <UserRang rang={rang} />
        </HeaderInfo>
      </Header>

      <UserSkills skills={skills} />
    </ListItem>
  );
};
