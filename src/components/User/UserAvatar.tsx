import { Avatar } from 'antd';
import { AvatarProps } from 'antd/lib/avatar';
import React, { FC } from 'react';

interface IComponentProps extends AvatarProps {
  avatar: string;
}

export const UserAvatar: FC<IComponentProps> = ({ avatar, ...rest }) => {
  return <Avatar src={avatar} {...rest} children="ПС" />;
};
