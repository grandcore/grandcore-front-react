import React, { FC } from 'react';
import styled from 'styled-components';
import { routes } from '../../routes';
import { FooterLink } from './FooterLink';

interface IComponentProps {}

const Wrapper = styled.footer`
  padding: 12px ${(p) => p.theme.indent.pdX};
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  background-color: ${(p) => p.theme.color.white};
  border-top: 1px solid #d1d1d1;
`;

const CopyRight = styled(FooterLink)`
  a {
    font-size: 12px;
  }
`;

export const Footer: FC<IComponentProps> = () => {
  return (
    <Wrapper>
      <FooterLink path={routes.about} children="О фонде" />

      <FooterLink path={routes.projects} children="Проекты" />

      <FooterLink path={routes.members} children="Участники" />

      <FooterLink path={routes.sponsors} children="Спонсоры" />

      {/* todo change to private policy route */}
      <FooterLink path={routes.home} children="Политика конфиденциальности" />

      <CopyRight path={routes.home} children="2019-2020 MIT" />
    </Wrapper>
  );
};
