import React, { FC } from 'react';
import Link from 'next/link';
import styled from 'styled-components';

interface IComponentProps {
  path: string;
  children: string;
  className?: string;
}

const Wrapper = styled.div`
  margin-right: 32px;

  :last-of-type {
    margin-right: 0;
  }

  a {
    font-size: 18px;
    font-weight: 600;
    color: ${(p) => p.theme.color.gray};

    &:hover {
      color: ${(p) => p.theme.color.accent};
    }
  }
`;

export const FooterLink: FC<IComponentProps> = ({ path, children, className }) => {
  return (
    <Wrapper className={className}>
      <Link href={path}>
        <a>{children}</a>
      </Link>
    </Wrapper>
  );
};
