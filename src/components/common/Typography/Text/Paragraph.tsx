import React, { FC } from 'react';
import styled from 'styled-components';

interface IComponentProps {
  children?: string;
}

const Wrapper = styled.p`
  font-size: 16px;
  font-weight: 500;
  color: ${(p) => p.theme.color.gray2};
`;

export const Paragraph: FC<IComponentProps> = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};
