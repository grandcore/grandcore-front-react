import styled from 'styled-components';

export const Headline1 = styled.h1`
  margin-bottom: 60px;
  font-size: ${(p) => p.theme.fontSize.header1};
  color: ${(p) => p.theme.color.gray};
`;
