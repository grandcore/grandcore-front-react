import styled from 'styled-components';

export const Headline2 = styled.h2`
  margin-bottom: 48px;
  font-size: ${(p) => p.theme.fontSize.header2};
  color: ${(p) => p.theme.color.gray};
`;
