import styled from 'styled-components';
import { Form } from 'antd';

export const StyledForm = styled(Form)`
  width: 320px;
  max-width: 480px;

  .ant-form-item {
    margin-bottom: 16px;

    &:last-child {
      margin-bottom: 0;
    }
  }

  .ant-form-item-control-input-content > * {
    padding: 8px 16px;
    height: auto;
    border-radius: 4px;
  }

  input {
    &[type='password'] {
      :hover,
      :active,
      :focus {
        box-shadow: none;
      }
    }
  }

  .ant-input {
    :hover,
    :active,
    :focus {
      border-color: ${(p) => p.theme.color.accent};
      box-shadow: 0 0 0 2px ${(p) => p.theme.color.accentShadow};
    }
  }

  .ant-input-affix-wrapper {
    :hover,
    :active,
    :focus {
      border-color: ${(p) => p.theme.color.accent};
      box-shadow: 0 0 0 2px ${(p) => p.theme.color.accentShadow};
    }

    &-focused {
      border-color: ${(p) => p.theme.color.accent};
      box-shadow: 0 0 0 2px ${(p) => p.theme.color.accentShadow};
    }
  }

  .ant-btn {
    :hover,
    :active,
    :focus {
      color: ${(p) => p.theme.color.accent};
      border-color: ${(p) => p.theme.color.accent};
    }
  }
`;
