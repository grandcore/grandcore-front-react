import React, { FC } from 'react';
import styled from 'styled-components';
import { Button } from 'antd';
import { BaseButtonProps } from 'antd/lib/button/button';

const StyledButton = styled(Button)`
  font-weight: 600;
  color: ${(p) => p.theme.color.white};
  border: none;
  background: ${(p) => p.theme.color.accent};
  border-radius: 8px;

  :hover,
  :focus,
  :active {
    color: ${(p) => p.theme.color.white};
    background: ${(p) => p.theme.color.accent};
  }
`;

interface IComponentProps extends BaseButtonProps {
  onClick?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  children: string;
}

export const PrimaryButton: FC<IComponentProps> = ({ onClick, children, ...rest }) => {
  return <StyledButton onClick={onClick} children={children} {...rest} />;
};
