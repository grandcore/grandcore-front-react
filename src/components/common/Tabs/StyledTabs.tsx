import React, { FC } from 'react';
import styled from 'styled-components';
import { Tabs } from 'antd';
import { TabsProps } from 'antd/lib/tabs';

const Wrapper = styled.div`
  .ant-tabs-ink-bar {
    background-color: ${(p) => p.theme.color.accent};
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: ${(p) => p.theme.color.accent};
  }

  .ant-tabs-tab,
  .ant-tabs-tab-btn {
    :hover,
    :active,
    :focus {
      color: ${(p) => p.theme.color.accent};
    }
  }
`;

interface IComponentProps extends TabsProps {}

export const StyledTabs: FC<IComponentProps> = ({ children, ...args }) => {
  return (
    <Wrapper>
      <Tabs {...args} children={children} />
    </Wrapper>
  );
};
