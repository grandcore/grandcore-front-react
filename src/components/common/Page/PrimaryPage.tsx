import styled from 'styled-components';

export const PrimaryPage = styled.section`
  max-width: 800px;
  margin: 0 auto;
  padding: 100px 0;

  flex: 1;
`;
