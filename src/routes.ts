export const routes = {
  home: '/',
  auth: '/auth',
  soft: '/soft',
  games: '/games',
  online: '/online',
  products: '/products',
  texts: '/texts',
  about: '/about',
  sponsors: '/sponsors',
  members: '/members',
  projects: '/projects',
};
