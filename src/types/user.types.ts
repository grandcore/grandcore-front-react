export interface IUserBody {}

export interface IUser {
  about: string;
  avatar: string;
  date_joined: string;
  email: string;
  first_name: string;
  group: number;
  id: number;
  is_active: boolean;
  is_deleted: boolean;
  is_staff: boolean;
  is_superuser: boolean;
  last_login: string;
  last_name: string;
  occupation: string;
  phone: string;
  rang: number;
  second_name: string;
  skills: string | Array<string>;
  telegram: string;
  updated: string;
}

export interface IUsersResponse {
  count: number;
  next: string;
  previous: string;
  results: Array<IUser>;
}
