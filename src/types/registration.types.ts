export interface IRegisterBody {
  email: string;
  password: string;
  invite: string;
}

export interface IRegisterResponse {}
