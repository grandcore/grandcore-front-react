const fontSize = {
  base: '16px',
  header1: '72px',
  header2: '48px',
};

const color = {
  primary: '#ccc',
  gray: '#333333',
  gray2: '#4F4F4F',
  gray3: '#828282',
  accent: '#00D681',
  accentShadow: '#00D68120',
  white: '#fff',
};

const indent = {
  pdX: '40px',
};

export const theme = {
  fontSize,
  color,
  indent,
};
