import React from 'react';
import styled from 'styled-components';
import { PrimaryPage } from '../components/common/Page/PrimaryPage';
import { Users } from '../components/Home/Users';

const StyledPrimaryPage = styled(PrimaryPage)`
  max-width: 1368px;
  display: flex;
  flex-flow: column nowrap;

  > * {
    margin-bottom: 150px;

    :last-child {
      margin-bottom: 0;
    }
  }
`;

// TEMPLATE
const MainSection = styled.div`
  height: calc(100vh - 200px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

const HomePage = () => {
  return (
    <StyledPrimaryPage>
      <MainSection>
        <h1>HomePage works!</h1>
      </MainSection>

      <Users />
    </StyledPrimaryPage>
  );
};

export default HomePage;
