import React from 'react';
import styled from 'styled-components';
import { AuthTabs } from '../../components/Auth/AuthTabs';
import { PrimaryPage } from '../../components/common/Page/PrimaryPage';

const StyledPrimaryPage = styled(PrimaryPage)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const AuthPage = () => (
  <StyledPrimaryPage>
    <AuthTabs />
  </StyledPrimaryPage>
);

export default AuthPage;
