import React from 'react';
import styled from 'styled-components';
import { PrimaryPage } from '../../components/common/Page/PrimaryPage';
import { Headline2 } from '../../components/common/Typography/Headline/Headline2';
import { Paragraph } from '../../components/common/Typography/Text/Paragraph';

const Wrapper = styled.div`
  margin-bottom: 50px;

  :last-child {
    margin-bottom: 0;
  }

  p {
    :last-child {
      margin-bottom: 0;
    }
  }

  h2 {
    :last-child {
      margin-bottom: 0;
    }
  }
`;

const LinksContainer = styled.div`
  display: grid;
  gap: 24px;
  grid-template-columns: repeat(3, 1fr);
`;

const LinksWrapper = styled.div`
  p {
    text-align: center;
  }

  a {
    font-size: 16px;
    font-weight: 500;
    color: ${(p) => p.theme.color.accent};

    :hover {
      color: ${(p) => p.theme.color.gray};
    }
  }
`;

const LinksTitle = styled.h3`
  font-size: 24px;
  font-weight: 500;
  text-align: center;
  color: ${(p) => p.theme.color.gray};
`;

const AboutPage = () => {
  return (
    <PrimaryPage>
      <Wrapper>
        <Headline2>Коротко о Фонде</Headline2>

        <Paragraph>
          Привет, как дела? Мы GrandCore Foundation, первая в мире Open Source корпорация. Наша цель — создать идеальную
          экосистему для работы над проектами под свободной лицензией, со всеми необходимыми для этого инструментами.
          Ищем Сооснователей!
        </Paragraph>
      </Wrapper>

      <Wrapper>
        <Headline2>Что мы делаем</Headline2>

        <Paragraph>
          В рамках фонда ведётся разработка и поддержка свободного ПО и игр, этичных альтернатив онлайн-сервисов,
          открытых стандартов изделий (от тостера до космической ракеты), проводятся исследования, идёт коллективная
          работа над статьями и книгами.
        </Paragraph>

        <Paragraph>
          Все проекты становятся общественным достоянием. Решения принимаются участниками фонда. Вес голоса
          пропорционален материальному и финансовому вкладу в проекты. Любое решение может быть заблокировано верховным
          советом в который входят Сооснователи и самые активные участники. Каждый член верховного совета имеет равный
          голос.
        </Paragraph>
      </Wrapper>

      <Wrapper>
        <Headline2>Реализация платформы</Headline2>

        <Paragraph>
          Работы ведутся в рамках специфичных для каждой задачи бизнес-процессов. При этом, их интерфейс условно можно
          разделить на 3 блока. В рабочем блоке формируются предложения для голосования или, например, предлагаются
          пулл-реквесты. Блок голосования позволяет отвергнуть или принять предложения, после чего выполнить
          соответствующие инструкции. Кроме того, пользователи могут обсуждать предложения в третьем блоке.
        </Paragraph>

        <Paragraph>
          Во время обсуждения, участник выдвинувший предложение, кураторы проекта и члены верховного совета могут
          производить изменения в рабочем блоке, опираясь на обратную связь, а так же отменить предложение. У каждого
          пользователя есть свой TODO лист, где отображаются все требующие его внимания процессы проектов на которые он
          подписан.
        </Paragraph>

        <Paragraph>
          Любой участник может предложить свой проект фонду. В отличие от GitHub и Kickstarter сообщество голосует за
          целесообразность добавления проекта в каталог, блокируя неинтересные проекты и проекты, которые дублируют уже
          существующие. Принятому проекту выбираются кураторы, на основе принятых сообществом соглашений выбирается
          стек.
        </Paragraph>

        <Paragraph>
          Работа над проектами ведётся по принципу конвейера. После завершения этапа, результат используется как
          отправная точка для следующего этапа. На первом этапе участники предлагают идею для новой фичи или фиксируют
          найденный баг. Любое предложение проходит этап голосования и в случае принятия, оно попадает в банк идей. На
          основе предложений пишутся техзадания для новых релизов. Разработчики предлагают свои кандидатуры для
          выполнения задач по текущим техзаданиям, указывая сроки и желаемый гонорар. В выделенное время сообщество
          контролирует ход разработки выбранного кандидата. Его работа может быть остановлена, в случае, если
          исполнитель будет работать некачественно.
        </Paragraph>

        <Paragraph>
          Сообщество каждого проекта имеет всю необходимую инфраструктуру. Участники смогут оказывать услуги по
          внедрению и доработке проектов фонда под нужды пользователей на коммерческой основе, продавать сделанные по
          стандартам изделия, их составные узлы и детали, работать над документацией и обучающими материалами, задавать
          вопросвы и отвечать на них.
        </Paragraph>
      </Wrapper>

      <Wrapper>
        <Headline2>Ссылки</Headline2>

        <LinksContainer>
          <LinksWrapper>
            <LinksTitle>Связь с основателем</LinksTitle>

            <p>
              <a href="https://t.me/grandcore">Telegram</a>
            </p>

            <p>
              <a href="https://vk.com/grandcore">Vk</a>
            </p>

            <p>
              <a href="https://www.linkedin.com/in/grandcore">LinkedIn</a>
            </p>

            <p>
              <a href="https://www.facebook.com/yo.grandcore">Facebook</a>
            </p>
          </LinksWrapper>

          <LinksWrapper>
            <LinksTitle>Информация</LinksTitle>

            <p>
              <a href="https://t.me/grandcore_news">Telegram - Новости и Объявления</a>
            </p>

            <p>
              <a href="https://www.youtube.com/channel/UCCcI0eNBhfd0qHIzZLDvKVA">YouTube канал</a>
            </p>
          </LinksWrapper>

          <LinksWrapper>
            <LinksTitle>Разработка</LinksTitle>

            <p>
              <a href="https://trello.com/user39645886">Задачи Trello</a>
            </p>

            <p>
              <a href="https://www.figma.com/file/NlikNEJQHliYlxI3MHhiSW/Share?node-id=2946%3A1036">Дизайн Figma</a>
            </p>

            <p>
              <a href="https://gitlab.com/grandcore">Репозиторий GitLab</a>
            </p>
          </LinksWrapper>
        </LinksContainer>
      </Wrapper>
    </PrimaryPage>
  );
};

export default AboutPage;
