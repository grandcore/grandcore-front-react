import React from 'react';
import 'antd/dist/antd.css';
import styled, { ThemeProvider } from 'styled-components';
import { AppProps } from 'next/app';
import { theme } from '../theme';
import { Header } from '../components/Header/Header';
import { GlobalStyle } from '../styles/GlobalStyle';
import { Footer } from '../components/Footer/Footer';

type Theme = typeof theme;

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {}
}

const Wrapper = styled.div`
  min-height: 100vh;
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
`;

const Main = styled.main`
  flex: 1;

  display: flex;
  flex-flow: column nowrap;
`;

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <Wrapper>
      <ThemeProvider theme={theme}>
        <GlobalStyle />

        <Header />

        <Main>
          <Component {...pageProps} />
        </Main>

        <Footer />
      </ThemeProvider>
    </Wrapper>
  );
};

export default App;
