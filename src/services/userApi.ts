import { notification } from 'antd';
import { IUsersResponse } from '../types/user.types';
import { BaseAPI } from './baseAPI';

class UserAPI extends BaseAPI {
  public async getUsers(): Promise<IUsersResponse> {
    return await this.baseAPI
      .get('/user/')
      .then((response) => response.data)
      .catch((error) => {
        console.error(error);

        switch (error.response.status) {
          case 401:
            return notification.error({ message: 'Авторизируйтесь!' });
        }
      });
  }
}

export const userAPI = new UserAPI();
