import { IAuthResponse } from './../types/auth.types';
import axios, { AxiosInstance } from 'axios';

export abstract class BaseAPI {
  protected readonly baseAPI: AxiosInstance;

  public constructor() {
    this.baseAPI = axios.create({
      baseURL: process.env.NEXT_PUBLIC_BASE_API_URL ?? 'http://localhost/api',
    });

    this.baseAPI.interceptors.request.use((config) => {
      const accessToken = JSON.parse(localStorage.getItem('accessToken'));

      if (accessToken) {
        config.headers.common.Authorization = `Bearer ${accessToken?.access}`;
      }

      return config;
    });
  }
}
