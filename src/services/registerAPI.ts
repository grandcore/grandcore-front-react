import { IRegisterResponse } from './../types/registration.types';
import { BaseAPI } from './baseAPI';
import { IRegisterBody } from '../types/registration.types';

class RegisterAPI extends BaseAPI {
  public async register(body: IRegisterBody): Promise<IRegisterResponse> {
    return await this.baseAPI
      .post('/register_user', body)
      .then((response) => response.data)
      .catch((error) => console.error(error));
  }
}

export const registerAPI = new RegisterAPI();
