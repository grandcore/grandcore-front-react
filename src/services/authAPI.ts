import { IAuthResponse } from './../types/auth.types';
import { BaseAPI } from './baseAPI';
import { IAuthBody } from '../types/auth.types';

class AuthAPI extends BaseAPI {
  public async login(body: IAuthBody): Promise<IAuthResponse> {
    return await this.baseAPI
      .post('/api/token/', body)
      .then((response) => response.data)
      .catch((error) => console.error(error));
  }
}

export const authAPI = new AuthAPI();
