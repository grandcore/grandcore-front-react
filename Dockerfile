FROM node:alpine

# Setup
RUN apk update
RUN apk upgrade

ENV APP_DIR /var/www/grandcore

# Initialize
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
ADD ./package.json .
ADD ./package-lock.json .

# Installing packages
RUN npm install

EXPOSE 3000

# Copy all
ADD . .

# Build project
RUN npm run build

ENTRYPOINT ["npm", "run"]
